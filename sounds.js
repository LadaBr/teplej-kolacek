const sounds = [
    {
        "keywords": ["curak"],
        "file": "curak.mp3",
    },
    {
        "keywords": ["klasika"],
        "file": { file: "Ak-ako-klasika-o.mp3", volume: 2 },
    },
    {
        "keywords": ["to jsem nevidel", "tos nevidel", "to nevidel", "to jsem nevidela"],
        "file": "no_tak_to_jsem_nevidel.mp3",
    },
    {
        "keywords": ["to je debil", "je to debil"],
        "file": "to_je_debil.mp3",
    },
    {
        "keywords": ["je mu hodne", "je ti hodne", "tobe je hodne", "neni mu nic"],
        "file": "neni_mu_nic_je_mu_hodne.mp3",
    },
    {
        "keywords": ["zdar"],
        "file": ["zdar zdar.mp3", "zdar.mp3", { file: "asimatyzdar.mp3", volume: 2 }],
    },
    {
        "keywords": ["holky"],
        "audio": { file: "holky.mp3", volume: 1 }
    },
    {
        "keywords": ["again", "agane"],
        "file": { file: "GO-AGANE.mp3", volume: 2 },
    },
    {
        "keywords": ["laska"],
        "file": { file: "laska.mp3", volume: 1 },
    },
    {
        "keywords": ["soul ring", "soulring"],
        "file": { file: "soulring.mp3", volume: 1.5 },
    },
    {
        "keywords": ["bez si tam vzit pusu", "bez si vzit pusu", "co"],
        "file": { file: "co.-bez-si-tam-vzit-pusu-voe.mp3", volume: 1 },
    },
    {
        "keywords": ["co se deje", "do prdele"],
        "file": { file: "co-se-deje.mp3", volume: 1 },
    },
    {
        "keywords": ["delej"],
        "file": { file: "delej.mp3", volume: 1 },
    },
    {
        "keywords": ["delej dolu", "pojd dolu", "dolu"],
        "file": { file: "delej_dolu.mp3", volume: 1 },
    },
    {
        "keywords": ["demolisn", "demolice", "znicim"],
        "file": { file: "DEMOLISN-NJEHEHEHE.mp3", volume: 1 },
    },
    {
        "keywords": ["drz hubu", "ztriskam drzku", "ztriskam ti drzku", "drz"],
        "file": { file: "Drz-hubu-vole-nebo-ti-vole....ztriskam-drzku-vole.....mp3", volume: 2 },
    },
    {
        "keywords": ["ele"],
        "file": { file: "ELE-PO-PRACI.mp3", volume: 1 },
    },
    {
        "keywords": ["je zivot", "co se da delat"],
        "file": { file: "Ele-to-je-zivot.mp3", volume: 1 },
    },
    {
        "keywords": ["vysoko"],
        "file": { file: "hodne-vysoko-miris.mp3", volume: 1 },
    },
    {
        "keywords": ["hovno"],
        "file": { file: "hovno.mp3", volume: 1 },
    },
    {
        "keywords": ["hovno hovno"],
        "file": { file: "hovno_hovno.mp3", volume: 1 },
    },
    {
        "keywords": ["jane", "ja ne"],
        "file": { file: "J8-NE.mp3", volume: 1 },
    },
    {
        "keywords": ["jdem tam"],
        "file": { file: "jdem.mp3", volume: 1 },
    },
    {
        "keywords": ["kapnul", "kapnout"],
        "file": { file: "kapnuti.mp3", volume: 1 },
    },
    {
        "keywords": ["kaves"],
        "file": { file: "KAVES.mp3", volume: 1 },
    },
    {
        "keywords": ["kek"],
        "file": { file: "KEKK.mp3", volume: 1 },
    },
    {
        "keywords": ["mnam"],
        "file": { file: "mnam-do-pici.mp3", volume: 1 },
    },
    {
        "keywords": ["lovaky"],
        "file": { file: "Lovy-lovaky-jho-tho-jhe.mp3", volume: 1 },
    },
    {
        "keywords": ["mrdam"],
        "file": { file: "mrdamtovoe.mp3", volume: 1 },
    },
    {
        "keywords": ["vmisit"],
        "file": { file: "Muzu-se-do-toho-vmisit.mp3", volume: 1 },
    },
    {
        "keywords": ["nadrzenej"],
        "file": { file: "NADRZENOST.mp3", volume: 1 },
    },
    {
        "keywords": ["vodmoderovat"],
        "file": { file: "Nechces-to-vodmoderovat-treba.mp3", volume: 1 },
    },
    {
        "keywords": ["pobrat dech"],
        "file": { file: "nemuze_pobrat_dech_ty_kryple.mp3", volume: 1 },
    },
    {
        "keywords": ["neurazil"],
        "file": { file: "Nevim-co-ti-mam-presne-odpovedet-abych-te-neurazil.mp3", volume: 1 },
    },
    {
        "keywords": ["no to mu je", "je mu neco", "mu neco je"],
        "file": { file: "no_to_mu_je_teda.mp3", volume: 1 },
    },
    {
        "keywords": ["trapeni"],
        "file": { file: "No-hele-co-mam-mam-se-s-tim-trapit.mp3", volume: 1 },
    },
    {
        "keywords": ["o"],
        "file": { file: "o.mp3", volume: 1, chance: 0.83, default_chance: 0.83 },
    },
    {
        "keywords": ["gachi"],
        "file": { file: "Oh_im_fucking_coming_-_Van_Darkholme.mp3", volume: 1 },
    },
    {
        "keywords": ["ohnono"],
        "file": { file: "ohnono.mp3", volume: 1 },
    },
    {
        "keywords": ["ooe"],
        "file": { file: "o-o-e.mp3", volume: 1 },
    },
    {
        "keywords": ["ooooe"],
        "file": { file: "OOOOE_online-audio-converter.com.mp3", volume: 1 },
    },
    {
        "keywords": ["pepelaugh"],
        "file": { file: "PepeLaugh.mp3", volume: 1 },
    },
    {
        "keywords": ["praskac"],
        "file": { file: "praskac.mp3", volume: 1 },
    },
    {
        "keywords": ["ded"],
        "file": { file: "roblox-death-sound-effect.mp3", volume: 1 },
    },
    {
        "keywords": ["rozcisnul"],
        "file": { file: "Rozcisnuti.mp3", volume: 1 },
    },
    {
        "keywords": ["rozlamanej"],
        "file": { file: "rozlamanej_jak_svine.mp3", volume: 1 },
    },
    {
        "keywords": ["fap"],
        "file": { file: "savage-fap.mp3", volume: 1 },
    },
    {
        "keywords": ["dokopu", "ser na to"],
        "file": { file: "Ser-na-to-pico-vole-nebo-te-tady-dokopu-ty-vole.mp3", volume: 1 },
    },
    {
        "keywords": ["svarta"],
        "file": { file: "svarta.mp3", volume: 1 },
    },
    {
        "keywords": ["sym"],
        "file": { file: "symjehovno.mp3", volume: 1 },
    },
    {
        "keywords": ["zena", "prebral zenu"],
        "file": { file: "TAK-VICEMENE....SEM-MU-PREBRAL-ZENU.mp3", volume: 1 },
    },
    {
        "keywords": ["domu"],
        "file": { file: "Taky-muzu-jit-domu-zejo.mp3", volume: 1 },
    },
    {
        "keywords": ["lose"],
        "file": { file: "thepriceisright-loserhorns.mp3", volume: 1 },
    },
    {
        "keywords": ["napsany"],
        "file": { file: "To-jako-nevim-hochu-jak-to-mas-napsany-no.mp3", volume: 1 },
    },
    {
        "keywords": ["train"],
        "file": { file: "train.mp3", volume: 1 },
    },
    {
        "keywords": ["ten ma dost"],
        "file": [{ file: "ty_curaku_ten_ma_dost.mp3", volume: 1 }, { file: "ty_debile_ten_ma_dost_jak_svine.mp3", volume: 1 }],
    },
    {
        "keywords": ["parchant"],
        "file": { file: "ty-mazanej-parchante.mp3", volume: 1 },
    },
    {
        "keywords": ["vejska"],
        "file": { file: "vejska_jako_prase.mp3", volume: 1 },
    },
    {
        "keywords": ["victory"],
        "file": { file: "victoryff.swf", volume: 1 },
    },
    {
        "keywords": ["vis"],
        "file": { file: "Vis-jak-to-je.mp3", volume: 1 },
    },
    {
        "keywords": ["vohen"],
        "file": { file: "vohen.mp3", volume: 1 },
    },
    {
        "keywords": ["co se deje", "cosedeje", "zvlastni veci"],
        "file": { file: "Vopravdu-zvlastni-veci.mp3", volume: 1 },
    },
    {
        "keywords": ["hic", "dobra parta"],
        "file": { file: "Vy-budete-dobra-parta-hic.mp3", volume: 1 },
    },
    {
        "keywords": ["vycepak"],
        "file": { file: "Vycepak.mp3", volume: 1 },
    },
    {
        "keywords": ["zdravim"],
        "file": { file: "zdravim.mp3", volume: 1 },
    },
    {
        "keywords": ["nevim", "neumim odpovedet"],
        "file": { file: "Neumim odpovědět.mp3", volume: 1 },
    },
];

module.exports = sounds;