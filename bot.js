const Discord = require('discord.js');
const client = new Discord.Client();
const config = require('./config');
const sounds = require('./sounds');
const ytdl = require('ytdl-core');

const channelBlacklist = [
    'CocaCola Hrušeň AFK'
];

let connection;

const command = {
    'didopice': async () => connection.disconnect()
}

const folder = 'audio/';

client.on('ready', () => {
    console.log(`Logged in as ${client.user.tag}!`);
});



const joinAndPlay = async (channel, itemsToPlay) => {
    for (const item of itemsToPlay) {
        const d = Math.random();
        if (!item.chance || d >= item.chance) {
            if (d > item.chance) {
                item.chance = item.default_chance;
            }
            let file, volume = 1;
            if (typeof item === 'string') {
                file = item;
            } else {
                file = item.file;
                volume = item.volume;
            }
            console.log('Joining channel', channel.name);
            try {
                connection = await channel.join();
                let stream;
                console.log('Playing', file, 'volume', volume);
                if (file.includes('https://www.youtube.com/')) {
                    stream = ytdl(file, { filter: 'audioonly' });
                } else {
                    stream = folder + file;
                }

                const dispatcher = await connection.play(stream, { volume });
                await new Promise(resolve => dispatcher.on('finish', () => {
                    console.log('Playback ended');
                    resolve();
                }));
            } catch (e) {
                console.error(e);
            }

        } else if (d < item.chance) {
            const diff = item.default_chance - item.chance;
            item.chance -= 0.05 + diff;
        }

    }
};

const playInChannelWithMention = async (channel, message, itemsToPlay) => {
    for (const [id, user] of message.mentions.users) {
        for (const [id, member] of channel.members) {
            if (member.user.id === user.id) {
                console.log('Found voice channel with mentioned user.');
                await joinAndPlay(channel, itemsToPlay);
                break;
            }
        }
    }
}
const queue = [];
client.on('message', async (message) => {
    const cmd = command[message.content];
    if (cmd) {
        await cmd(message);
    } else {
        queue.push(message);
    }

});

const processMessage = async (message) => {
    const content = message.content.normalize("NFD").replace(/[\u0300-\u036f]/g, "").toLowerCase();
    const itemsToPlay = [];
    for (const item of sounds) {
        const match = item.keywords.some(k => content.match(`[ ]?${k}[ ]?(?!\\w+)`));
        if (match) {
            let file;
            if (Array.isArray(item.file)) {
                file = item.file[Math.floor(Math.random() * item.file.length)]
            } else {
                file = item.file;
            }
            itemsToPlay.push(file);
        }
    }
    if (itemsToPlay.length) {
        for (const [id, channel] of message.guild.channels.cache) {
            if (channel.type === 'voice' && channel.members.size > 0 && !channelBlacklist.some(ch => ch === channel.name)) {
                if (message.mentions.users.size) {
                    await playInChannelWithMention(channel, message, itemsToPlay);
                } else {
                    await joinAndPlay(channel, itemsToPlay);
                }
            }
        }
    }
}

(async () => {
    while (true) {
        if (queue.length) {
            await processMessage(queue[0]);
            queue.splice(0, 1);
        }

        await new Promise(resolve => setTimeout(resolve, 50));
    }
})().catch(e => console.error(e));

client.login(config.token);